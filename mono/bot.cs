using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;

public class Bot {
	public static string bot_name;
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        bot_name = args[2];
        string botKey = args[3];	
		string trackName = "";
		
		if (args.Length == 5)
			trackName = args[4];
		 
		Console.WriteLine("Connecting to " + host + ":" + port + " as " + bot_name + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, bot_name, botKey, trackName);
		}
	}
	

	private StreamWriter writer;
	
	private Race race;
	PathFinder path_finder;
	private bool path_guide_updated = false;
	private float[,] path_guide = null;
	private int current_piece = -1;
	private int index_of_me = -1;
	
	private float current_speed = 0;
	private float current_acc = 0;
	private float current_throttle = 0;
	private int current_lane = -1;
	private int current_piece_index = -1;	
	
	private float last_speed = 0;
	private float last_acc = 0;
	private float last_throttle = 0;
	private int last_lane = -1;
	private int last_piece_index = -1;
	private float last_inpiece_distance = 0;
	
	private float fSlip = 0.417f;
	private float targetSpeed = 0;
	private int numPieceToNextCurve = 0;
	private float distanceToNextCurve = 0;
	private PiecePosition myPiecePosition;
	private float brakeTime = 22;
	private int currentLap;
	private int maxLap;
	private float distanceToNextLap;
	/*
		Game init
	*/
	
	public void onGameInit(MsgWrapper msg)
	{
		Console.WriteLine("Race init");
		string data = msg.data.ToString().Replace("\"switch\": ", "\"_switch\": ");
		RaceData race_data = JsonConvert.DeserializeObject<RaceData>(data);
		race = race_data.race;
		path_finder = new PathFinder();
		path_finder.BuildDistance(race.track);
		path_finder.BuildCrossCost(race.track);
		send(new Ping());
		last_speed = 0;
		current_speed = 0;
		last_inpiece_distance = 0;
		currentLap = 1;
		maxLap = race.raceSession.laps;
	}
	/*
		Car position
	*/
	public void onCarPosition(MsgWrapper msg)
	{
		IList<CarPosition> position = JsonConvert.DeserializeObject<IList<CarPosition>>(msg.data.ToString());
		findMe(position);
		
		if(current_lane != -1)
		{
			last_lane = current_lane;
			last_piece_index = current_piece_index;
			last_speed = current_speed;
		}
		else//Start game
		{			
			last_speed = 0;
			current_speed = 0;
			last_acc = 0;
			current_acc = 0;
			last_throttle = 0;
			current_throttle = 0;
		}
		
		myPiecePosition =position[index_of_me].piecePosition;
		current_lane = myPiecePosition.lane.startLaneIndex;
		current_piece_index = myPiecePosition.pieceIndex;		
		
		
		findPath(current_piece_index, current_lane);		
		
		if (current_piece_index == last_piece_index)
		{
			current_speed = myPiecePosition.inPieceDistance - last_inpiece_distance;
			last_inpiece_distance = myPiecePosition.inPieceDistance;
		}
		else if (last_piece_index != -1)
		{
			float dist = race.track.pieces[last_piece_index].length - last_inpiece_distance;
			if (dist > 0 )
				current_speed = myPiecePosition.inPieceDistance + dist;
			last_inpiece_distance = myPiecePosition.inPieceDistance;
		}
		
		last_acc = current_acc;
		current_acc = current_speed - last_speed;
		
		// send command before meet the switch
		int new_piece = position[0].piecePosition.pieceIndex+1;
		if(new_piece != race.track.pieces.Count)
		if(new_piece != current_piece && race.track.pieces[new_piece]._switch)
		{
			Console.WriteLine("Switch Piece {0}",new_piece);
			current_piece = new_piece;
			updateSwitch();
		}
		
		calculatedistanceToNextLap();
		calculateNextCurveData();
		
		if ((distanceToNextLap <= distanceToNextCurve) && currentLap == maxLap)
		{
			send(new Throttle(1));
		}
		else
		{
			float future_acc = current_acc + (current_acc - last_acc);
			float timeToNextCurve = distanceToNextCurve/(current_speed+future_acc);
			
			if (timeToNextCurve < brakeTime)
			{
				if (current_speed < targetSpeed) 
					send(new Throttle(1));
				else if(current_speed - targetSpeed < 0.00001)
					send(new Throttle(0.5));
				else
					send(new Throttle(0.0));
			}
			else
				send(new Throttle(1));
		}
		Console.WriteLine("v: {0},target: {1}", current_speed, targetSpeed);
	}
	
	/*
		Crash		
	*/
	public void onCrash(MsgWrapper msg)	
	{		
		CarID car = JsonConvert.DeserializeObject<CarID>(msg.data.ToString());
		string name = car.name;
		if (name == bot_name)
			Console.WriteLine("Our car {0} has crashed", bot_name);		
		else
			Console.WriteLine("{0} has crashed", name);		
	}
	
	public void onSpawn(MsgWrapper msg)
	{
		CarID car = JsonConvert.DeserializeObject<CarID>(msg.data.ToString());
		string name = car.name;
		if (name == bot_name)
			Console.WriteLine("Our car {0} has spawn", bot_name);		
		else
			Console.WriteLine("{0} has spawn", name);		
		current_speed = 0;		
		last_speed = 0;
	}	

	public void onTurbo(MsgWrapper msg)
	{	
		Console.WriteLine("Turbo avaiable,distanceToNextCurve:{0}", distanceToNextCurve);
		float speed = Math.Min(current_speed*2, 17f);
		float timeToNextCurve = distanceToNextCurve/speed;
		Console.WriteLine("Turbo avaiable,timeToNextCurve:{0}", timeToNextCurve);
		if (timeToNextCurve > 15||((distanceToNextLap <= distanceToNextCurve) && currentLap == maxLap))
		{	
			send(new Turbo("Bump it!!!"));
			Console.WriteLine("Use turbo");
		}
	}	
	
	
	public void calculatedistanceToNextLap()
	{
		int totalPiece = race.track.pieces.Count;
		int nextPiece = current_piece_index;
		int index = nextPiece;
		distanceToNextLap = race.track.pieces[current_piece_index].length - myPiecePosition.inPieceDistance;
		if (distanceToNextLap < 0)
		{			
			distanceToNextLap = 0;
			nextPiece++;
		}		
		for(int i = 0; i < totalPiece - 1; i++)
		{
			index = (nextPiece + i)%totalPiece;
			if (i > 0)
				distanceToNextLap = distanceToNextLap + race.track.pieces[i].length;
			if((index+1)%totalPiece == 0)
				break;
		}
	}
	
	public void calculateNextCurveData()
	{
		int totalPiece = race.track.pieces.Count;
		int nextPiece = current_piece_index%totalPiece;
		int index = nextPiece;
		distanceToNextCurve = race.track.pieces[current_piece_index].length - myPiecePosition.inPieceDistance;		
		if (distanceToNextCurve < 0)
		{			
			distanceToNextCurve = 0;
			nextPiece++;
		}
		for(int i = 0; i < totalPiece - 1; i++)
		{
			index = (nextPiece + i)%totalPiece;
			if (i > 0)
				distanceToNextCurve = distanceToNextCurve + race.track.pieces[i].length;
			if(race.track.pieces[index].angle !=0)
				break;
		}		 	
		targetSpeed = (float)Math.Sqrt((race.track.pieces[index].radius)*fSlip);		
	}
	
	public float getNextCurveMaxSpeed(int index)
	{	
		return (float)Math.Sqrt((race.track.pieces[index].radius)*fSlip);		
	}	
	
	public void updateSwitch()
	{
		int guide = (int)path_guide[current_piece,0];
		if(guide != 0)
		{
			string switch_data = guide==-1?"Left":"Right";
			send(new SwitchLane(switch_data));
			//Console.WriteLine("Switch "+switch_data);
		}
		else
		{
			//Console.WriteLine("Switch Straight");
		}
	}
	
	Bot(StreamReader reader, StreamWriter writer, string name, string key, string trackName) {
		this.writer = writer;
		string line;
		
		if (trackName == "")
		{
			send(new Join(name, key));
		}
		else
		{
			send(new JoinRace(name, key, trackName));
		}
		

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
					onCarPosition(msg);
					break;
				case "crash":
					onCrash(msg);
					break;
				case "spawn":
					onSpawn(msg);
					break;
				case "turboAvailable":
					onTurbo(msg);
					break;								
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					onGameInit(msg);
					break;
				case "lapFinished":
					currentLap++;
					path_guide_updated = false;
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	public void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());

	}
	public void findMe(IList<CarPosition> positions)
	{
		if(index_of_me != -1) return;
		for(int i=0;i<positions.Count;i++)
		{
			if(positions[i].id.name == bot_name)
			{
				index_of_me = i;
				return;
			}
		}
	}
	public void findPath(int piece, int lane)
	{
		if(path_guide_updated) return;
		//Console.WriteLine("PATH FINDER BEGIN");
		//Console.WriteLine("Find path travel through pieces 0 to piece max");
		path_finder.Query(piece,lane,race.track.pieces.Count-1, ref path_guide);
		//float length = path_finder.Query(piece,lane,race.track.pieces.Count-1, ref path_guide);
		//Console.WriteLine("length = {0}",length);
		//Console.WriteLine("path guide = ");
		for(int i=0;i<path_guide.GetLength(0);i++)
		{
			//if(race.track.pieces[i]._switch)
			//Console.Write("[action {0}, cost {1}], ", path_guide[i,0], path_guide[i, 1]);
		}
		//Console.WriteLine();
		//Console.WriteLine("PATH FINDER TEST END");
		path_guide_updated = true;
		current_piece = 0;
		updateSwitch();
	}
}

public class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

public abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

public class BotID
{
	public string name;
	public string key;
	
	public BotID(string name, string key) {
		this.name = name;	
		this.key = key;		
	}	
}

public class JoinRace: SendMsg {		
	public string trackName;
	public int carCount;	
	public BotID botId;

	public JoinRace(string name, string key, string trackName) {
		this.botId = new BotID(name, key);
		this.trackName = trackName;
		this.carCount = 1;
	}

	protected override string MsgType() { 
		return "joinRace";
	}
}

public class Join: SendMsg {		
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;		
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

public class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

public class Throttle: SendMsg {
	public double value;
	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}
public class SwitchLane: SendMsg {
	public string value;

	public SwitchLane(string value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "switchLane";
	}
}

public class Turbo: SendMsg {
	public string data;

	public Turbo(string msg) {
		this.data = msg;
	}

	protected override Object MsgData() {
		return this.data;
	}

	protected override string MsgType() {
		return "turbo";
	}
}