using System;
public class PathFinder
{
	// distance[i,j] = distance from start to piece i+1, if go straight on lane j
	public float[,] distance;
	public void BuildDistance(Track track)
	{
		distance = new float[track.pieces.Count,track.lanes.Count];
		//Console.WriteLine("Build distance");
		for(int i=0;i<track.pieces.Count;i++)
		{
			Piece piece = track.pieces[i];
			for(int j=0;j<track.lanes.Count;j++)
			{
				float len;
				Lane lane = track.lanes[j];
				if(piece.angle == 0)
				{
					len = piece.length;
				}
				else
				{
					float dr = lane.distanceFromCenter * (piece.angle>0?-1:1);
					float r = piece.radius + dr;
					len = r * (float)Math.PI * (float)Math.Abs(piece.angle) / 180.0f;
					if (len < piece.length||(piece.length == 0))
						piece.length = len;
				}
				if(i == 0)
				{
					distance[i,j] = len;
				}
				else
				{
					distance[i,j] = distance[i-1,j] + (float)Math.Abs(len);
				}
				//if(piece._switch) Console.Write("* ");
				//Console.Write("{0}, ",distance[i,j]);
			}
			//Console.WriteLine();
		}
	}
	// cross_cost[i,j] = cost to cross j <-> j+1, at piece i, if can't cross, return 0;
	public float[,] cross_cost;
	public void BuildCrossCost(Track track)
	{
		cross_cost = new float[track.pieces.Count+1,track.lanes.Count-1];
		Console.WriteLine("Build cross cost");
		for(int i=0;i<track.pieces.Count;i++)
		{
			Piece piece = track.pieces[i];
			if(!piece._switch) continue;
			for(int j=1;j<track.lanes.Count;j++)
			{
				Lane lane1 = track.lanes[j];
				Lane lane2 = track.lanes[j-1];
				float c;
				if(piece.radius == 0)
				{
					float a = (float)Math.Abs(lane1.distanceFromCenter - lane2.distanceFromCenter);
					float b = piece.length;
					float c2 = a*a + b*b;
					c = (float)Math.Sqrt(c2);
					//Console.Write("a = {0}, b = {1}, ", a, b);
				}
				else
				{
					float da = lane1.distanceFromCenter * (piece.angle>0?-1:1);
					float a = piece.radius + da;
					float db = lane2.distanceFromCenter * (piece.angle>0?-1:1);
					float b = piece.radius + db;
					Console.Write("radius = {2}, a = {0}, b = {1}, ", a,b,piece.radius);
					float alpha_radian = (float)Math.PI * piece.angle / 180.0f;
					Console.Write("alpha = {0}, ", alpha_radian);
					Console.Write("cos a = {0}, ", (float)Math.Cos(alpha_radian));
					float c2 = a*a + b*b - 2*a*b*(float)Math.Cos(alpha_radian);
					c = (float)Math.Sqrt(c2);
				}
				cross_cost[i,j-1] = c;
				Console.Write("c = {0}, ",c);
			}
			Console.WriteLine();
		}
	}
	// return cheapest cost to get to finish_piece from piece start_piece, lane start_lane
	public float Query(int start_piece, int start_lane, int finish_piece, ref float[,] result)
	{
		int piece_count = finish_piece + 1;
		int lane_count = distance.GetLength(1);
		float[,] f = new float[piece_count,lane_count];
		int[,] trace = new int[piece_count,lane_count];
		// build
		//Console.WriteLine("build f");
		for(int i=start_piece;i<=finish_piece;i++)
		{
			for(int j = 0;j<lane_count;j++)
			{
				if(i==start_piece && j != start_lane)
				{
					f[i,j] = float.MaxValue;
					continue;
				}
				float min_f;
				float local_f;
				// straight
				local_f = (i!=0?f[i-1,j]:0) + distance[i,j] - (i!=0?distance[i-1,j]:0);
				//Console.Write("l {0}, ",local_f);
				//Console.Write("d {0}, ",distance[i,j]);
				min_f = local_f;
				trace[i,j] = 0;
				// switch right
				if(j != 0)
				{
					if(cross_cost[i,j-1] != 0)
					{
						local_f = (i!=0?f[i-1,j-1]:0) + cross_cost[i,j-1];
						if(local_f < min_f)
						{
							min_f = local_f;
							trace[i,j] = 1;
						}
					}
				}
				// switch left
				if(j != lane_count - 1)
				{
					if(cross_cost[i,j] != 0)
					{
						local_f = (i!=0?f[i-1,j+1]:0) + cross_cost[i,j];
						if(local_f < min_f)
						{
							min_f = local_f;
							trace[i,j] = -1;
						}
					}
				}
				f[i,j] = min_f;
				Console.Write("{0}, ",min_f);
			}
			//Console.WriteLine();
		}
		// trace
		result = new float[piece_count,2];
		int piece = finish_piece;
		int lane = 0;
		float best_f = f[piece,lane];
		
		for(int j = 0;j<lane_count;j++)
		{
			if(f[piece,j] < best_f)
			{
				lane = j;
				best_f = f[piece,j];
			}
		}
		//Console.WriteLine("final lane {0}",lane);
		float ret = best_f;
		while(piece != start_piece)
		{
			result[piece,0] = trace[piece,lane];
			result[piece,1] = f[piece,lane];
			//Console.WriteLine("piece {0} lane {1} switch to get to lane {2}", piece, lane, trace[piece,lane]);
			lane -= (int)trace[piece,lane];
			piece--;
		}
		return ret;
		// result array = array of int in {0,1,-1} 0 = no switch, 1 = switch right, -1 = switch left
		// return tour length
	}
}