using System;
using System.Collections;
using System.Collections.Generic;
public class Point
{
	public float x{ get;set; }
	public float y{ get;set; }
}
public class StartingPoint
{
	public Point position{ get;set; }
	public float angle{ get;set; }
}
public class Piece
{
	public float length{ get;set;}
	public bool _switch{ get;set; }
	public float radius{ get;set; }
	public float angle{ get;set; }
}
public class Lane
{
	public int distanceFromCenter{ get;set; }
	public int index{ get;set; }
}
public class Track
{
	public string id{ get;set; }
	public string name{ get;set; }
	public IList<Piece> pieces{ get;set; }
	public IList<Lane> lanes{ get;set; }
	public StartingPoint startingPoint;
}
public class CarID
{
	public string name{ get;set; }
	public string color{ get;set; }
}
public class CarDimension
{
	public float length{ get;set; }
	public float width{ get;set; }
	public float guideFlagPosition{ get;set; }
}
public class Car
{
	public CarID id{ get;set; }
	public CarDimension dimensions{ get;set; }
}
public class RaceSession
{
	public int laps{ get;set; }
	public int maxLapTimeMs{ get;set; }
	public bool quickRace{ get;set; }
}
public class Race
{
	public Track track{ get;set; }
	public IList<Car> cars{ get;set; }
	public RaceSession raceSession{ get;set; }
}
public class RaceData
{
	public Race race{ get;set; }
}
public class LaneSwitchInfo
{
	public int startLaneIndex{ get;set; }
	public int endLaneIndex{ get;set; }
}
public class PiecePosition
{
	public int pieceIndex{ get;set; }
	public float inPieceDistance{ get;set; }
	public LaneSwitchInfo lane{ get;set; }
	public int lap{ get;set; }
}
public class CarPosition
{
	public CarID id{ get;set; }
	public float angle{ get;set; }
	public PiecePosition piecePosition{ get;set; }
}
public class TurboInfo
{
	public float turboDurationMilliseconds{get;set; }
	public float turboDurationTicks{get;set; }
	public float turboFactor{get;set; }
}